<?php
$fileToday = 'dataToday.csv';
$fileYesterday = 'dataYesterday.csv';
$fileParso = 'dataParso.csv';
$file3months = 'data3months.csv';
$file6months = 'data6months.csv';

function initialize($stockArray, $stockDataRoot) {
  foreach ($stockArray as $stock) {
    init($stock->name, $stockDataRoot);
  }
}

function init($stock, $stockDataRoot) {
  $stockPath = $stockDataRoot."/".$stock;
  if (! file_exists($stockPath)) {
    mkdir($stockPath);
  }
  global $fileToday, $fileYesterday, $fileParso, $file3months, $file6months;
  $fToday = $stockDataRoot."/".$stock."/".$fileToday;
  $fYesterday = $stockDataRoot."/".$stock."/".$fileYesterday;
  $fParso = $stockDataRoot."/".$stock."/".$fileParso;
  $f3months = $stockDataRoot."/".$stock."/".$file3months;
  $f6months = $stockDataRoot."/".$stock."/".$file6months;
    
  $data = file_get_contents(getGoogleFinanceURI($stock, "3d", 60));
  $days = processData($data);
  file_put_contents($fParso, $days[0]);
  file_put_contents($fYesterday, "1,".$days[1]);
  file_put_contents($fToday, "1,".$days[2]);
  
  $data3months = file_get_contents(getGoogleFinanceURI($stock, "3M", 1800));
  file_put_contents($f3months, processxMonthsData($data3months));
  
  $data6months = file_get_contents(getGoogleFinanceURI($stock, "6M", 1800));
  file_put_contents($f6months, processxMonthsData($data6months));
}

function getGoogleFinanceURI($stock, $days, $interval) {
  $uri = "https://www.google.com/finance/getprices?i=$interval&p=$days&f=d,c&df=cpct&q=$stock";
  return $uri;
}

function processData($data) {
  $data = clarifyData($data);
  $days = explode("\n1,", $data);
  return $days;
}

function processxMonthsData($data) {
  $data = clarifyData($data);
  $rawArray = explode("\n", $data);
  $processedArray = array();
  for ($i = 0; $i < count($rawArray); $i++) {
    $row = $rawArray[$i];
    $row = preg_replace("/.*,/", "", $row);
    
    if (empty($row)) {
      continue;
    }
    array_push($processedArray, ($i+1).",".$row);
  }
  
  return implode("\n", $processedArray);
}

function clarifyData($data) {
  $patterns = array();
  $patterns[0] = '/^EXCHANGE.*\n/';
  $patterns[1] = '/^MARKET.*\n/';
  $patterns[2] = '/^MARKET.*\n/';
  $patterns[3] = '/^INTERVAL.*\n/';
  $patterns[4] = '/^COLUMNS.*\n/';
  $patterns[5] = '/^DATA.*\n/';
  $patterns[6] = '/\nA.*/i'; //should come before TIMEZONE, otherwise '\n' pattern won't work
  $patterns[7] = '/^TIMEZONE.*\n/';
  
  return preg_replace($patterns, "", $data);
}

function getData() {
  global $file;
  return file_get_contents($file);
}