<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

include('processor.php');

class stock {
  var $name = '';
  var $color = '';
  
  function __construct($name) {
    $this->name = $name;
  }
}

$stockDataRoot = "stock-data";
$stocks = array(new stock("AMZN"), new stock("NVDA"), new stock("SQ"), new stock("MSFT"), new stock("BAC"));
initialize($stocks, $stockDataRoot);
?>
<html>
<style>
div {
  display: inline-block;
}  
</style>
<head>
  <script src="//cdnjs.cloudflare.com/ajax/libs/dygraph/2.0.0/dygraph.min.js"></script>
  <link rel="stylesheet" src="//cdnjs.cloudflare.com/ajax/libs/dygraph/2.0.0/dygraph.min.css" />
</head>

<body>
  Stocks dashboard for <b>Sanjay</b><br/><br/>
  
  <?php foreach($stocks as $stock) { ?>
  <table border="1">
    <b><?php print $stock->name ?>
      <?php print "[<a target=\"_blank\" href=\"https://www.google.com/finance?q=NYSE:$stock->name\">google finance</a>]" ?>
      <?php print "[<a target=\"_blank\" href=\"https://www.google.com/search?q=$stock->name%20stock\">google search</a>]" ?>
    </b>
   <tr>
      <td>
         <table border="1">
            <tr>
               <td colspan="2">
                 Today
                  <div id="<?php print $stock->name ?>graphdivToday" style="width:400px; height:270px"></div>
               </td>
            </tr>
            <tr>
               <td>
                 Yesterday
                  <div id="<?php print $stock->name ?>graphdivYesterday" style="width:200px; height:135px"></div>
               </td>
               <td>
                 Day before yesterday
                  <div id="<?php print $stock->name ?>graphdivParso" style="width:200px; height:135px"></div>
               </td>
            </tr>
         </table>
      </td>
      <td>3 months<div id="<?php print $stock->name ?>graphdiv3M" style="width:400px; height:270px"></div></td>
      <td>6 months<div id="<?php print $stock->name ?>graphdiv6M" style="width:400px; height:270px"></div></td>
    </tr>
  </table>
  <?php } ?>

  <script type="text/javascript">
    var stocks = [<?php foreach ($stocks as $stock) print "\"".$stock->name."\","; ?>];
    //stocks = ["AMZN", "NVDA"]
    var stock = "stock-data/AMZN/";
    
    for (index in stocks) {
      var stock = stocks[index]
      var stockPath = "stock-data/" + stock + "/"
      console.log(stockPath)
      g = new Dygraph(document.getElementById(stock + "graphdivToday"), stockPath + "dataToday.csv", {});
      g = new Dygraph(document.getElementById(stock + "graphdivYesterday"), stockPath + "dataYesterday.csv", {});
      g = new Dygraph(document.getElementById(stock + "graphdivParso"), stockPath + "dataParso.csv", {});
      g = new Dygraph(document.getElementById(stock + "graphdiv3M"), stockPath + "data3months.csv", {});
      g = new Dygraph(document.getElementById(stock + "graphdiv6M"), stockPath + "data6months.csv", {});
    }
  </script>
</body>

</html>